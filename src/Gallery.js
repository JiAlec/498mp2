import React from 'react'
import axios from 'axios'

export default function Gallery({pokeList, setLookupMon }) {
    return (
        <div>
            {pokeList.map(link => (
                <div key={link} onClick={e => 
                    setLookupMon(link)
                }>
                    <img src={link}/>
                </div>
            ))

            }
        </div>
    )
}
