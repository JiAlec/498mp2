import React, { useState, useEffect } from 'react';
import Pokedex from './Pokedex'
import Pagination from './Pagination'
import Search from './Search'
import Modal from './Modal'
import Gallery from './Gallery'
import axios from 'axios'
import { BrowserRouter as Router } from "react-router-dom";
import './App.css';

function App() {
  const [pokemon, setPokemon] = useState([])
  const [currentPageUrl, setCurrentPageUrl] = useState("https://pokeapi.co/api/v2/pokemon?limit=151")
  const [nextPageUrl, setNextPageUrl] = useState()
  const [prevPageUrl, setPrevPageUrl] = useState()
  const [searchPoke, setSearchPoke] = useState("");
  const [loading, setLoading] = useState(true)
  const [lookupMon, setLookupMon] = useState("bulbasaur")
  const [lookupURL, setLookupURL] = useState("https://pokeapi.co/api/v2/pokemon/bulbasaur")
  const [lookUpData, setLookupData] = useState({})
  const [galleryUrl, setGalleryUrl] = useState("https://pokeapi.co/api/v2/pokemon?limit=9")
  const [galleryList, setGalleryList] = useState([])
  const [galleryImgs, setGalleryImgs] = useState([])

  const sortNumAsc = true
  const sortNumDesc = false
  const sortNameAsc = false
  const sortNameDesc = false

  const filterPosts = (query) => {
    if (!query) {
        return pokemon;
    }

    return pokemon.filter((poke) => {
        return poke.includes(query);
    });
  };

  const { search } = window.location;
  const query = new URLSearchParams(search).get('s');
  const [searchQuery, setSearchQuery] = useState('');
  const filteredPokes = filterPosts(searchQuery);

  // var state = {
  //   show: false
  // };
  // function showModal = e => {
  //   this.setState({
  //     show: true
  //   });
  // };

  useEffect(() => {
    setLoading(true)
    let cancel
    axios.get(currentPageUrl, {
      cancelToken: new axios.CancelToken(c => cancel = c)
    }).then(res => {
      setNextPageUrl(res.data.next)
      setPrevPageUrl(res.data.previous)
      setPokemon(res.data.results.map(poke => poke.name))
      setLoading(false)
    })

    return () => {
      cancel()
    }
  }, [currentPageUrl])

  useEffect(() => {
    let cancel
    axios.get(galleryUrl, {
      cancelToken: new axios.CancelToken(c => cancel = c)
    }).then(res => {
      setGalleryList(res.data.results.map(poke => poke.name))
    })

    return () => {
      cancel()
    }
  }, [galleryUrl])

  useEffect(() => {
    let cancel
    var lst = []
    for (var i = 0; i < galleryList.length; i++) {
      axios.get("https://pokeapi.co/api/v2/pokemon-form/" + galleryList[i], {
        cancelToken: new axios.CancelToken(c => cancel = c)
      }).then(res => {
        lst.push(res.data.sprites.front_default)
      })
    }

    setGalleryImgs(lst)
  }, [galleryList])

  useEffect(() => {
    setLookupURL("https://pokeapi.co/api/v2/pokemon/" + lookupMon)
    axios.get(lookupURL).then(res => {
      setLookupData({
        "Name" : res.data.name,
        "Type" : res.data.types[0].type.name,
        "Height" : res.data.height,
        "HP" : res.data.stats[0].base_stat
      })
    })
  }, [lookupMon])

  function goNext() {
    setCurrentPageUrl(nextPageUrl)
  }

  function goPrev() {
    setCurrentPageUrl(prevPageUrl)
  }

  if (loading) {
    return <div className="App">Loading ...</div>
  }

  return (
    <Router>
      <div className = "App">
        <Search 
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery} 
        />
        <div className = "Pokedex">
          <Pokedex pokemon={filteredPokes ? filteredPokes : pokemon} setLookupMon={setLookupMon}/>
        </div>
        <Pagination
          goNext={nextPageUrl ? goNext : null}
          goPrev={prevPageUrl ? goPrev : null}
        />
        <Modal data = {lookUpData}/>
        <br></br>
        <div className = "Gallery">
          <Gallery galleryList={galleryList} pokeList={galleryImgs} setLookupMon={setLookupMon}/>
        </div>
      </div>
    </Router>
  );
}

export default App;
