import React from 'react'

export default function Pokedex({ pokemon, setLookupMon }) {
    return (
        <div>
            {pokemon.map(poke => (
                <div className = "entry" key={poke} onClick={e => 
                    setLookupMon(poke)
                }>{poke}</div>
            ))}
        </div>
    )
}
