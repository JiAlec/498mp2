import React from 'react'
import { useHistory } from 'react-router-dom';

export default function Search({ searchQuery, setSearchQuery }) {
    const history = useHistory();
    const onSubmit = e => {
        history.push(`?s=${searchQuery}`)
        e.preventDefault()
    };

    return <form action="/" method="get" autoComplete="off" onSubmit={onSubmit}>
        <input
                    value={searchQuery}
                    onInput={e => setSearchQuery(e.target.value)}
                    type="text"
                    id="header-search"
                    placeholder="Search Pokedex"
                    name="s" 
                />
                <button type="submit">Search</button>
    </form>
}
